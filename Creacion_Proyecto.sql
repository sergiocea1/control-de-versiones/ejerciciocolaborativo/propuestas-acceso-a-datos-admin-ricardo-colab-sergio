-- Creamos la base de datos

CREATE DATABASE proyecto_inicial
CHARACTER SET latin1
COLLATE latin1_spanish_ci;

-- Cantantes

CREATE TABLE proyecto_inicial.cantantes (
  cod int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(20) DEFAULT NULL,
  apellido varchar(20) DEFAULT NULL,
  PRIMARY KEY (cod)
);

-- Festivales

CREATE TABLE proyecto_inicial.festivales (
  cod int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(20) DEFAULT NULL,
  fecha_inicio date DEFAULT NULL,
  fecha_fin date DEFAULT NULL,
  PRIMARY KEY (cod)
);

-- Clientes

CREATE TABLE proyecto_inicial.clientes (
  dni varchar(20) NOT NULL,
  nombre varchar(20) DEFAULT NULL,
  apellido varchar(20) DEFAULT NULL,
  correo varchar(20) DEFAULT NULL,
  fecha_nacimiento date DEFAULT NULL,
  edad int(11) DEFAULT NULL,
  PRIMARY KEY (dni)
);

-- Conciertos 

CREATE TABLE proyecto_inicial.conciertos (
  cod int(11) NOT NULL AUTO_INCREMENT,
  fecha date DEFAULT NULL,
  hora_inicio time DEFAULT NULL,
  hora_fin time DEFAULT NULL,
  cod_festivales int(11) NOT NULL,
  PRIMARY KEY (cod, cod_festivales)
);

-- Cantan

CREATE TABLE proyecto_inicial.cantan (
  cod_cant int(11) NOT NULL,
  cod_conciertos int(11) NOT NULL,
  cod_festivales int(11) NOT NULL,
  PRIMARY KEY (cod_cant, cod_conciertos, cod_festivales)
);

-- Entradas 

CREATE TABLE proyecto_inicial.entradas (
  cod int(11) NOT NULL AUTO_INCREMENT,
  num_entrada int(11) DEFAULT NULL,
  cod_festivales int(11) NOT NULL,
  PRIMARY KEY (cod, cod_festivales)
);

-- Compran

CREATE TABLE proyecto_inicial.compran (
  cod_entradas int(11) NOT NULL,
  cod_fest int(11) NOT NULL,
  cod_festivales int(11) NOT NULL,
  dni varchar(20) NOT NULL,
  PRIMARY KEY (cod_entradas, cod_festivales, dni, cod_fest)
);

-- Instrumentos

CREATE TABLE proyecto_inicial.instrumentos (
  cod_conciertos int(11) NOT NULL,
  cod_festivales int(11) NOT NULL,
  instrumentos varchar(20) NOT NULL,
  PRIMARY KEY (instrumentos, cod_conciertos, cod_festivales)
);

-- Restricciones

ALTER TABLE proyecto_inicial.conciertos
ADD CONSTRAINT FK_conciertos_cod_festivales FOREIGN KEY (cod_festivales)
REFERENCES proyecto_inicial.festivales (cod) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.entradas
ADD CONSTRAINT FK_entradas_festivales FOREIGN KEY (cod_festivales)
REFERENCES proyecto_inicial.festivales (cod) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.cantan
ADD CONSTRAINT FK_cantan_cod_cant FOREIGN KEY (cod_cant)
REFERENCES proyecto_inicial.cantantes (cod) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.cantan
ADD CONSTRAINT FK_cantan_cod_conciertos FOREIGN KEY (cod_conciertos)
REFERENCES proyecto_inicial.conciertos (cod) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.cantan
ADD CONSTRAINT FK_cantan_cod_festivales FOREIGN KEY (cod_festivales)
REFERENCES proyecto_inicial.festivales (cod) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.compran
ADD CONSTRAINT FK_compran_cod_entradas FOREIGN KEY (cod_entradas)
REFERENCES proyecto_inicial.entradas (cod) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.compran
ADD CONSTRAINT FK_compran_cod_fest FOREIGN KEY (cod_fest)
REFERENCES proyecto_inicial.festivales (cod) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.compran
ADD CONSTRAINT FK_compran_cod_festivales FOREIGN KEY (cod_festivales)
REFERENCES proyecto_inicial.festivales (cod) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.compran
ADD CONSTRAINT FK_compran_dni FOREIGN KEY (dni)
REFERENCES proyecto_inicial.clientes (dni) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.compran
ADD UNIQUE INDEX UK_compran (dni, cod_festivales);

ALTER TABLE proyecto_inicial.compran
ADD UNIQUE INDEX UK_compran2 (cod_entradas, cod_fest, cod_festivales);

ALTER TABLE proyecto_inicial.instrumentos
ADD CONSTRAINT FK_instrumentos_cod_conciertos FOREIGN KEY (cod_conciertos)
REFERENCES proyecto_inicial.conciertos (cod) ON DELETE NO ACTION;

ALTER TABLE proyecto_inicial.instrumentos
ADD CONSTRAINT FK_instrumentos_cod_festivales FOREIGN KEY (cod_festivales)
REFERENCES proyecto_inicial.festivales (cod) ON DELETE NO ACTION;





